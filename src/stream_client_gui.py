import sys
import asyncio
import json
import urllib.parse

from qtpy import QtWidgets, QtCore, QtGui

import asyncqt

import nats

from stream_entity_cmd import random_cmd


class Output(QtWidgets.QTextEdit):
    link_clicked = QtCore.Signal(str)

    def __init__(self, *args, **kwargs):
        super(Output, self).__init__(*args, **kwargs)
        self._pressed_anchor = None

    def mousePressEvent(self, event):
        anchor = self.anchorAt(event.pos())
        if anchor:
            self._pressed_anchor = anchor
        super().mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        anchor = self.anchorAt(event.pos())
        if anchor and anchor == self._pressed_anchor:
            self.link_clicked.emit(anchor)
        self._pressed_anchor = None
        super().mouseReleaseEvent(event)


class Window(QtWidgets.QWidget):
    def __init__(self, parent):
        super(Window, self).__init__(parent)
        self._nc = None
        self._js = None

        self.setLayout(QtWidgets.QHBoxLayout())
        button_lo = QtWidgets.QVBoxLayout()
        self.layout().addLayout(button_lo)

        output_lo = QtWidgets.QVBoxLayout()
        self.layout().addLayout(output_lo)

        b = QtWidgets.QPushButton(self)
        button_lo.addWidget(b)
        b.setText("Connect")
        b.clicked.connect(self.connect)

        b = QtWidgets.QPushButton(self)
        button_lo.addWidget(b)
        b.setText("Get Assets")
        b.clicked.connect(self.get_assets)

        b = QtWidgets.QPushButton(self)
        button_lo.addWidget(b)
        b.setText("Get Shots")
        b.clicked.connect(self.get_shots)

        b = QtWidgets.QPushButton(self)
        button_lo.addWidget(b)
        b.setText("Get Tasks")
        b.clicked.connect(self.get_tasks)

        b = QtWidgets.QPushButton(self)
        button_lo.addWidget(b)
        b.setText("Send Random CMD")
        b.clicked.connect(self.send_random_entity_cmd)

        button_lo.addStretch()

        self._out = Output(self)
        self._out.link_clicked.connect(self._on_link)
        output_lo.addWidget(self._out)

    def clear_output(self):
        self._out.moveCursor(QtGui.QTextCursor.End)
        self._out.clear()

    def print(self, *args):
        parts = []
        for i in args:
            i = str(i)
            if i.startswith("link:"):
                try:
                    name, url = i[len("link:") :].split("@", 1)
                except ValueError:
                    pass  # was not a link directive...
                else:
                    i = f'<a href="{url}">{name}</a>'
            parts.append(i)
        self._out.append(" ".join(parts))

    @asyncqt.asyncSlot()
    async def _on_link(self, url):
        print("Link", url)
        parsed = urllib.parse.urlparse(url)
        if parsed.scheme == "slot":
            slot = getattr(self, parsed.netloc)
            kwargs = dict(urllib.parse.parse_qsl(parsed.query))
            for k, v in kwargs.items():
                if v == "None":
                    kwargs[k] = None
            self.clear_output()
            await slot(**kwargs)

    @asyncqt.asyncSlot()
    async def connect(self):
        if self._nc is None:
            self.print("Connecting...")
            self._nc = await nats.connect()
            self._js = self._nc.jetstream()

            self.print("Conneted.")
        else:
            self.print("Already conneted !")

    @asyncqt.asyncSlot()
    async def get_assets(self, asset_type=None):
        self.clear_output()
        if self._nc is None:
            await self.connect()

        payload = json.dumps(dict(asset_type=asset_type)).encode()
        future = self._nc.request("entities.asset_tree", payload)
        msg = await future
        asset_tree = json.loads(msg.data.decode())
        title = (asset_type or "assets").title()
        self.print(f"<H2>{title}</H2>")
        for asset_type in asset_tree:
            etype = asset_type["asset_type"]
            self.print(f"<h3>#--{etype}</h3>")
            for asset in asset_type["assets"]:
                self.print(
                    f"&nbsp; &nbsp;{asset}",
                    f"link:(tasks)@slot://get_tasks?parent_etype={etype}&parent_eid={asset}",
                )

    @asyncqt.asyncSlot()
    async def get_shots(self, sequence_eid=None):
        self.clear_output()
        if self._nc is None:
            await self.connect()

        payload = json.dumps(dict(sequence=sequence_eid)).encode()
        future = self._nc.request("entities.shot_tree", payload)
        msg = await future
        shot_tree = json.loads(msg.data.decode())
        title = sequence_eid or "Shots"
        self.print(f"<H2>{title}</H2>")
        for seq in shot_tree:
            seq_eid = seq["sequence"]
            self.print(f"<h3>#--{seq_eid}</h3>")
            for shot in seq["shots"]:
                self.print(
                    f"&nbsp; &nbsp;{shot}",
                    f"link:(tasks)@slot://get_tasks?parent_etype=shots&parent_eid={shot}",
                )

    @asyncqt.asyncSlot()
    async def get_tasks(self, parent_etype="shots", parent_eid="sq001_sh004"):
        self.clear_output()
        if self._nc is None:
            await self.connect()

        payload = json.dumps(
            dict(parent_etype=parent_etype, parent_eid=parent_eid)
        ).encode()
        future = self._nc.request("entities.entity_tasks", payload)
        msg = await future
        tasks = json.loads(msg.data.decode())
        nb = len(tasks)
        self.print(
            f"<H2>Tasks</H2><H3>Got {nb} tasks for {parent_etype} {parent_eid}</H3>"
        )
        for task in tasks:
            self.print("  ", task)
        if parent_etype == "shots":
            self.print(f"link:back to shots@slot://get_shots?sequence_eid=None")
            seq = parent_eid.split("_")[0]
            self.print(f"link:go to {seq}@slot://get_shots?sequence_eid={seq}")
        else:
            self.print(f"link:show all chars@slot://get_assets?asset_type=chars")
            self.print(f"link:show all sets@slot://get_assets?asset_type=sets")
            self.print(f"link:show all props@slot://get_assets?asset_type=props")
            self.print(f"link:back to assets@slot://get_assets?asset_type=None")

    @asyncqt.asyncSlot()
    async def send_random_entity_cmd(self):
        self.clear_output()
        if self._nc is None:
            await self.connect()

        subject = "entities.cmds"
        cmd = random_cmd()
        self.print("Sending cmd to", subject)
        self.print(" Cmd:", cmd)
        ack = await self._js.publish(subject, json.dumps(cmd).encode())
        self.print(f"  Ack: stream={ack.stream}, sequence={ack.seq}")


def run(argv):
    app = QtWidgets.QApplication(argv)
    loop = asyncqt.QEventLoop(app)
    asyncio.set_event_loop(loop)

    w = Window(None)
    w.show()

    # app.exec_()
    with loop:
        loop.run_forever()


if __name__ == "__main__":
    run(sys.argv)
