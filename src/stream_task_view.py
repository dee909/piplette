import asyncio
import json
from collections import defaultdict

import nats


class DB(object):
    _THE = None

    @classmethod
    def get(cls):
        if cls._THE is None:
            cls._THE = cls()
        return cls._THE

    def __init__(self):
        super(DB, self).__init__()
        self._by_entity = defaultdict(set)
        self._by_task = {}

    def reparent(self, task_eid, parent_etype, parent_eid):
        try:
            old_parent_etype, old_parent_eid = self._by_task[task_eid]
        except KeyError:
            pass
        else:
            try:
                self._by_entity[(old_parent_etype, old_parent_eid)].remove(task_eid)
            except KeyError:
                print("!!!!! Inconsistency in task index !!!!!")

        self._by_entity[(parent_etype, parent_eid)].add(task_eid)
        self._by_task[task_eid] = (parent_etype, parent_eid)

        print("=", task_eid)

    def get_tasks_of(self, parent_etype, parent_eid):
        return list(self._by_entity.get((parent_etype, parent_eid), []))


async def handle_entity_updated(js):
    async def on_entity_updated(msg):
        info = json.loads(msg.data.decode())
        etype = info["etype"]
        attr = info["attr"]
        if etype != "tasks" or attr != "parent":
            return

        task_eid = info["eid"]
        parent_etype, parent_eid = info["value"]
        if parent_etype not in task_eid:
            # those are from a previous implementation, we should
            # hide those from the rest of the world:
            return

        DB.get().reparent(task_eid, parent_etype, parent_eid)

    # Ephemeral subscriber with instant replay all
    await js.subscribe(
        subject="entities.updated",
        cb=on_entity_updated,
        ordered_consumer=True,
    )


async def on_req_entity_tasks(msg):
    args = json.loads(msg.data.decode())
    parent_etype = args.get("parent_etype")
    parent_eid = args.get("parent_eid")
    if None in (parent_etype, parent_eid):
        return []

    print("REQ tasks for", parent_etype, parent_eid)
    tasks = DB.get().get_tasks_of(parent_etype, parent_eid)

    resp = json.dumps(tasks).encode()
    await msg.respond(resp)


async def handle_req_entity_tasks(nc: nats.NATS):
    await nc.subscribe("entities.entity_tasks", cb=on_req_entity_tasks)


async def main():
    nc = await nats.connect()
    js = nc.jetstream()

    await handle_entity_updated(js)
    await handle_req_entity_tasks(nc)

    try:
        await asyncio.Event().wait()
    except (KeyboardInterrupt, SystemExit):
        pass


if __name__ == "__main__":
    asyncio.run(main())
