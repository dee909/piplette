import asyncio
import nats
import nats.aio.msg
import nats.js
import json
from collections import defaultdict

from kv_entity_store import Store

SHOT_STORE = None


class Entity(object):
    def __init__(self, etype, eid):
        super(Entity, self).__init__()
        self.etype = etype
        self.eid = eid
        self._attrs = {}

    def update(self, attr, value):
        self._attrs[attr] = value
        # print(self.etype, self.eid, attr, value)

    def get(self, name, *default):
        try:
            return self._attrs[name]
        except KeyError:
            if not default:
                raise
            default = default[0]
            self._attrs[name] = default
            return default

    def matches(self, filter=None):
        """
        see DB.get_all for filter format
        """
        # print("???", self.etype, self.eid, filter)
        if filter is None:
            return True
        filter = dict(
            filter
        )  # copy to avoid in place edit (should this be a deepcopy ???)
        try:
            eid = filter.pop("eid")
        except KeyError:
            pass
        else:
            if eid is not None and eid != self.eid:
                return False

        for attr, value in filter.items():
            try:
                v = self._attrs[attr]
            except KeyError:
                return False
            if v != value:
                return False

        return True


class DB(object):
    _THE = None

    @classmethod
    def get(cls):
        if cls._THE is None:
            cls._THE = cls()
        return cls._THE

    def __init__(self):
        super(DB, self).__init__()
        self._entities = defaultdict(dict)

    def get_or_create(self, etype, eid):
        created = False
        entity = self._entities[etype].get(eid)
        if entity is None:
            entity = Entity(etype, eid)
            self._entities[etype][eid] = entity
            created = True
            print("+Entity", etype, eid)
        return entity, created

    def update_entity(self, etype, eid, attr, value):
        entity, created = self.get_or_create(etype, eid)
        entity.update(attr, value)
        return created

    def get_all(self, etype, filter=None, sort_by_eid=False):
        """
        Return all entities of etype matching the filter.
        Filter in the form {eid: pattern, attr1:pattern, attr2:pattern, ...}
        where everything is AND'ed.
        """
        entities = self._entities[etype]
        matched = [entity for entity in entities.values() if entity.matches(filter)]
        if sort_by_eid:
            matched.sort(key=lambda e: e.eid)
        return matched


async def auto_create_sequence(shot_eid):
    db = DB.get()
    shot_entity, created = db.get_or_create("shots", shot_eid)
    if created:
        await on_entity_created("shots", shot_eid)

    # Ensure the sequence exists:
    try:
        seq_id, _ = shot_eid.split("_")
    except ValueError:
        seq_id = "shots"

    seq, created = DB.get().get_or_create("sequences", seq_id)
    if created:
        await on_entity_created("sequences", seq_id)

    # Ensure the shot is registered in the sequence:
    # Also ensure shots are sorted by name
    shots = seq.get("shots", [])
    if shot_entity not in shots:
        # print("Seq added shot", eid)
        shots.append(shot_entity)
        # sort shots:
        shots.sort(key=lambda x: int(x.eid.split("_")[-1][2:]))  # sooooo ugly ^^'


async def on_entity_cmd(msg: nats.aio.msg.Msg):
    # print("Entity:", msg.data)
    cmd = json.loads(msg.data)
    db = DB.get()
    if cmd["cmd"] == "set":
        created = db.update_entity(
            cmd["etype"],
            cmd["eid"],
            cmd["attr"],
            cmd["value"],
        )
        if created:
            if cmd["etype"] == "shots":
                await auto_create_sequence(cmd["eid"])
            await on_entity_created(cmd["etype"], cmd["eid"])
        await on_entity_updated(cmd["etype"], cmd["eid"], cmd["attr"], cmd["value"])


async def on_req_shot_tree(msg):
    args = json.loads(msg.data.decode())
    print("REQ shot_tree", args)

    if "sequence" in args:
        seq_eid = args.pop("sequence")
        args["eid"] = seq_eid

    print("getting sequences", args)
    sequences = DB.get().get_all("sequences", filter=args, sort_by_eid=True)
    tree = []
    for seq in sequences:
        shot_eids = []
        for shot in seq.get("shots"):
            shot_eids.append(shot.eid)
        tree.append(dict(sequence=seq.eid, shots=shot_eids))

    resp = json.dumps(tree).encode()
    await msg.respond(resp)


async def on_req_asset_tree(msg):
    args = json.loads(msg.data.decode())
    print("REQ asset_tree", args)

    etypes = ["chars", "props", "sets"]
    if "asset_type" in args:
        etype = args.pop("asset_type")
        if etype is not None:
            etypes = [etype]

    print("getting assets", etypes)
    tree = []
    for etype in etypes:
        assets = DB.get().get_all(etype, sort_by_eid=True)
        tree.append(dict(asset_type=etype, assets=[asset.eid for asset in assets]))

    resp = json.dumps(tree).encode()
    await msg.respond(resp)


async def on_entity_created(etype, eid):
    await _PUBLISH_ENTITY_EVENT("created", etype, eid)


async def on_entity_updated(etype, eid, attr, value):
    await _PUBLISH_ENTITY_EVENT("updated", etype, eid, attr=attr, value=value)


_PUBLISH_ENTITY_EVENT = None


async def setup_on_entity_envent(js):
    global _PUBLISH_ENTITY_EVENT
    await js.add_stream(
        name="entities_events",
        subjects=[
            "entities.created",
            "entities.updated",
            "entities.deleted",
        ],
    )

    async def publish_entity_event(event_type, etype, eid, **more_info):
        data = dict(etype=etype, eid=eid)
        data.update(more_info)
        ack = await js.publish(
            f"entities.{event_type}",
            json.dumps(data).encode(),
        )
        print(f"Ack: stream={ack.stream}, sequence={ack.seq}")

    _PUBLISH_ENTITY_EVENT = publish_entity_event


async def handle_entities_cmds(js: nats.js.JetStreamContext):
    # Ephemeral subscriber with instant replay all
    await js.subscribe(
        subject="entities.cmds",
        cb=on_entity_cmd,
        ordered_consumer=True,
    )


async def handle_req_shot_tree(nc: nats.NATS):
    await nc.subscribe("entities.shot_tree", cb=on_req_shot_tree)


async def handle_req_asset_tree(nc: nats.NATS):
    await nc.subscribe("entities.asset_tree", cb=on_req_asset_tree)


async def main():
    nc = await nats.connect()
    js = nc.jetstream()

    await setup_on_entity_envent(js)

    await handle_entities_cmds(js)
    await handle_req_shot_tree(nc)
    await handle_req_asset_tree(nc)

    try:
        await asyncio.Event().wait()
    except (KeyboardInterrupt, SystemExit):
        pass


if __name__ == "__main__":
    asyncio.run(main())
