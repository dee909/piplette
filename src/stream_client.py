import asyncio
import json

import nats


async def main():
    nc = await nats.connect()

    if 0:
        # get the shot tree:
        payload = json.dumps(dict(sequence="sq003")).encode()
        payload = json.dumps(dict(sequence=None)).encode()
        future = nc.request("entities.shot_tree", payload)
        msg = await future
        shot_tree = json.loads(msg.data.decode())
        print("Got shot tree:")
        for seq in shot_tree:
            print("#--", seq["sequence"])
            for shot in seq["shots"]:
                print("  ", shot)

    if 0:
        # get the asset tree:
        payload = json.dumps(dict(asset_type="chars")).encode()
        payload = json.dumps(dict(asset_type=None)).encode()
        future = nc.request("entities.asset_tree", payload)
        msg = await future
        asset_tree = json.loads(msg.data.decode())
        print("Got asset tree:")
        for asset_type in asset_tree:
            print("#--", asset_type["asset_type"])
            for asset in asset_type["assets"]:
                print("  ", asset)

    if 1:
        # get the tasks of an entity:
        parent_etype = "chars"
        parent_eid = "bob"
        parent_etype = "shots"
        parent_eid = "sq001_sh004"
        payload = json.dumps(
            dict(parent_etype=parent_etype, parent_eid=parent_eid)
        ).encode()
        future = nc.request("entities.entity_tasks", payload)
        msg = await future
        tasks = json.loads(msg.data.decode())
        print(f"Got tasks for {parent_etype} {parent_eid}:")
        for task in tasks:
            print("  ", task)


if __name__ == "__main__":
    asyncio.run(main())
