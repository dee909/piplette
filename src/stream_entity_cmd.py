import asyncio
import random
import time
import json

import nats
import nats.js.api


def random_shot_id():
    seq = random.randint(1, 10)
    shot = random.randint(1, 20)
    return f"sq{seq:03}_sh{shot:03}"


def random_asset_id(asset_type):
    names = dict(
        chars=["bill", "bob", "boule"],
        props=["glass", "chair", "table"],
        sets=["kitchen", "bar"],
    )
    return random.choice(names[asset_type])


def random_cmd():
    shot_attrs = ["start", "end", "status"]
    asset_attrs = ["has_fur", "status"]
    etypes = {
        "shots": shot_attrs,
        "chars": asset_attrs,
        "sets": asset_attrs,
        "props": asset_attrs,
    }
    etype = random.choice(list(etypes.keys()))
    if etype == "shots":
        eid = random_shot_id()
    else:
        eid = random_asset_id(etype)
    attr = random.choice(etypes[etype])
    value = random.randint(1, 50)

    return dict(cmd="set", etype=etype, eid=eid, attr=attr, value=value)


async def main():
    nc = await nats.connect()
    js = nc.jetstream()

    await js.add_stream(
        name="entities_cmds",
        subjects=[
            "entities.cmds",
        ],
    )

    try:
        for i in range(100):
            subject = "entities.cmds"
            cmd = random_cmd()
            print(subject, cmd)
            ack = await js.publish(subject, json.dumps(cmd).encode())
            print(f"  Ack: stream={ack.stream}, sequence={ack.seq}")
            time.sleep(0.5)
    except KeyboardInterrupt:
        pass
    finally:
        await nc.close()


if __name__ == "__main__":
    asyncio.run(main())
