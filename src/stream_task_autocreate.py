import asyncio
import json

import nats


async def handle_entity_created(js):
    async def on_entity_created(msg):
        # print("entity cmd", msg)
        info = json.loads(msg.data.decode())
        etype = info["etype"]
        eid = info["eid"]

        tasks_by_parent_etype = dict(
            shots=["anim", "lighting", "render", "comp"],
            chars=["mod", "rig", "look", "gym"],
            sets=[
                "cammap",
                "mod",
                "look",
            ],
            props=["mod", "look"],
        )
        try:
            tasks_to_create = tasks_by_parent_etype[etype]
        except KeyError:
            # no tasks for this type of entities
            return

        for task_name in tasks_to_create:
            subject = "entities.cmds"
            task_eid = f"{eid}_{etype}_{task_name}"
            cmd = dict(
                cmd="set",
                etype="tasks",
                eid=task_eid,
                attr="parent",
                value=(etype, eid),
            )
            ack = await js.publish(subject, json.dumps(cmd).encode())
            # print(f"  Ack: stream={ack.stream}, sequence={ack.seq}")
            print("+task", task_eid)

    # Ephemeral subscriber with instant replay all
    await js.subscribe(
        subject="entities.created",
        cb=on_entity_created,
        ordered_consumer=True,
    )


async def main():
    nc = await nats.connect()
    js = nc.jetstream()

    await handle_entity_created(js)

    try:
        await asyncio.Event().wait()
    except (KeyboardInterrupt, SystemExit):
        pass


if __name__ == "__main__":
    asyncio.run(main())
