# piplette - LGPL License

PoC for a Reactive Studio Pipeline using nats.io

# Usage

## Run NATS Server

Go to https://nats.io/download/ and install the NATS server.
Start the server with JetStream enabled:
`nats-server --jetstream`

## Prepare a virtual env

Clone this repo, cd into in, create a virtual env (python 3.10 recommanded), ativate it, then enter:
`pip install -r requires.txt`

## Create Entities

Open a new shell, activate the virtual env, cd to your piplette clone and enter:

`python src/stream_entity_cmd.py`

This Service will emit 200 random commands which create and update some assets and shots.

## Materialize Entities

Open a new shell, activate the virtual env, cd to your piplette clone and enter:

`python src/stream_entity_view.py`

This Service listens to entity commands and materialize them in memory. 
When creating or updating an entity, it emits messages that other services can listen to in order to react to entity modifications.

This Servie creates sequence entitie automatically when a shot with a sequence in its name is created.

This Service also listen for resquest:
- `entities.shot_tree`: return some shot grouped by sequence.
- `entities.asset_tree`: return some assets grouped by asset type

## Create Tasks

Open a new shell, activate the virtual env, cd to your piplette clone and enter:

`python src/stream_task_autocreate.py`

This Service listens to entity creation and emits command to create tasks. The created tasks depend on the type of entity: shots and props don't have the same tasks.

## Materialize Entity Tasks

Open a new shell, activate the virtual env, cd to your piplette clone and enter:

`python src/stream_task_view.py`

This Service links tasks with their parent entity. Tasks are entities like shot and assets, but they have a parent attribute containing the type and the id of their parent. This Service listens to entities update and, in the case of an update of the parent attribute of a task, updates the links between the task and its (new) parent.

This Service also listen for requests:
- `entities.entity_tasks`: return all the tasks for a given entity type and id.

## Browse Entities

Open a new shell, activate the virtual env, cd to your piplette clone and enter:

`python src/stream_client_gui.py`

This will show a window with some buttons and a text area.
Clicking the button will request things from the services and display the result in the text area. You can click the links in the text to show related things...

This GUI uses NATS (which is async) into a Qt app (which is sync) thanks to `asyncqt`.

## Play Disaster Monkey

You can Ctrl-C some of the services and see how it turns when you spin them up again. 

Spoiler: everything goes well and no command is lost.

The reason is that all issued commands are stored in a stream that is read again at services startup. The materialization services always rebuild their memory databases exactly as it was before the crash.

## Clean up 

In order to restart from scratch, you'll need to delete some or all stream on the NATS server.

Download the `nats` CLI here: https://github.com/nats-io/natscli/releases/

Enter `nats stream rm` to be presented a list of streams:

    ? Select a Stream  [Use arrows to move, type to filter]
    > entities_cmds
    entities_events

You can select `entities_events` and delete it, re-launch your services and you will get back to the same state.

If you select `entities_cmds` and delete it, you will need to re-run the `stream_entity_cmd.py` service to create new entities.

